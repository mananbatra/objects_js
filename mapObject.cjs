//const testObj=require('./testObject.cjs');

function mapObject(obj, callBack) {
    
  
    for (let key in obj) 
    {
      
        obj[key] = callBack(obj[key]);
      
    }
  
    return obj;
  }

module.exports=mapObject;